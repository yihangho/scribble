#Scribble
Copyright 2013 Yihang Ho  
##Start Scribbling Now!
[http://scribble.yihangho.com](http://scribble.yihangho.com "Scribble")
##Introduction
This project is inspired by the famous pastebin.com. The idea of sharing plain text documents just be using a shortened link is amazing. Scribble is very similar to pastebin.com, except it is more math oriented - users can share not only plain text documents, but also math. Users can use a combination of HTML and LaTeX to typeset their document. The combination of these two essentially allows any document to be created. Craftier users can even create interactive documents with the help of JavaScript.
##Open Source
Scribble is an open source project. Besides, it is powered by several others Open Source project, as listed below, in alphabetical order:

* [Font Awesome](http://fortawesome.github.io/Font-Awesome/ "Font Awesome")
* [jQuery](http://jquery.com/ "jQuery")
* [MathJax](http://www.mathjax.org/ "MathJax")
* [Twitter Bootstrap](http://twitter.github.io/bootstrap/index.html "Twitter Bootstrap")
* [Zocial CSS Social Button](http://zocial.smcllns.com/ "Zocial CSS Social Buttons")

##License
Scribble is released under the [MIT License](http://opensource.org/licenses/mit-license.php "MIT License")  
Copyright (c) 2013 Yihang Ho

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
