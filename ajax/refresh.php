<?php

include '../mysql/mysql.php';

if (!array_key_exists('id', $_POST))
{
	echo json_encode(array(
		"status" => "fail",
		"error" => 0
	));
	die();
}

$id = $_POST['id'];

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."scribble WHERE id='$id'");

if (!$result->num_rows)
{
	echo json_encode(array(
		"status" => "fail",
		"error" => 6
	));
}

$row = $result->fetch_assoc();
$result->free_result();
$row['status'] = "pass";

echo json_encode($row);

?>