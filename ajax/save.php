<?php

include '../mysql/mysql.php';

if (!array_key_exists('id', $_POST) || !array_key_exists('title', $_POST) || !array_key_exists('content', $_POST))
{
	echo json_encode(array(
		"status" => "fail",
		"error" => 0
	));
	die();
}

$id = $_POST['id'];
$title = $_POST['title'];
$content = $_POST['content'];

if ($title == "")
	$title = "(Untitled Scribble)";

$content = $mysql_db->real_escape_string($content);

if ($id == "-1")
{
	do{
		$ukey = '$'.md5(uniqid(rand(), TRUE));
		$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."scribble WHERE ukey='$ukey'");
		$cont = $result->num_rows;
		$result->free_result();
	} while ($cont);

	$long_url = APPS_HOME.$ukey;

	$post_data = array(
		"longUrl" => $long_url
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/urlshortener/v1/url?key=".GOOGLE_API);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
	$google_response = curl_exec($ch);
	curl_close($ch);
	$response = json_decode($google_response, true);
	$short_url = $response['id'];

	$mysql_db->query("INSERT INTO ".MYSQL_PREFIX."scribble (title, content, ukey, short_url) VALUES('$title', '$content', '$ukey', '$short_url')");
	$id = $mysql_db->insert_id;
}
else
	$mysql_db->query("UPDATE ".MYSQL_PREFIX."scribble SET title='$title', content='$content' WHERE id='$id'");

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."scribble WHERE id='$id'");
$row = $result->fetch_assoc();
$row['status'] = "pass";

echo json_encode($row);

?>