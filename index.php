<?php
include 'mysql/mysql.php';

$ukey = "";
$id = "";
$title = "";
$content = "";
$short_url = "";

if (array_key_exists('ukey', $_REQUEST))
{
	$ukey = $_REQUEST['ukey'];
	$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."scribble WHERE ukey='$ukey'");
	if (!$result->num_rows)
	{
		header("Location: index.php");
		die();
	}
	$row = $result->fetch_assoc();
	$result->free_result();
	$id = $row['id'];
	$title = $row['title'];
	$content = $row['content'];
	$short_url = $row['short_url'];
}
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>Scribble<?php if ($ukey != ""):?> - <?php echo $title;?><?php endif;?></title>

		<!--Load CSS.-->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/zocial.css">

		<!--Load JS. For those that have the option to use CDN, please still include a copy for development use and include the code to import them while have them commented.-->
		<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
		<script src="js/jquery-1.9.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src='http://connect.facebook.net/en_US/all.js'></script>
		<script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
		<script type="text/x-mathjax-config">
			MathJax.Hub.Config({
				showProcessingMessages: false,
				tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
			});
		</script>
		<script>
			var editor_mode = <?php if ($ukey == ""):?>true<?php else:?>false<?php endif;?>;
		</script>
		<script src="js/script.js"></script>
	</head>

	<body>
		<div id="tooltip-container"></div>
		<div class="row-fluid" style="margin-top: 5px; margin-bottom: 5px;">
			<div class="span8 offset2">
				<div class="input-prepend" id="short-url-container"<?php if ($ukey == ""):?> style="display:none;"<?php endif;?>>
					<span class="add-on">
						<?php if (strpos($_SERVER["HTTP_USER_AGENT"], 'Macintosh')):?>
							&#8984;+C
						<?php else:?>
							Ctrl+C
						<?php endif;?>
					</span>
					<input id="short-url" type="text" value="<?php echo $short_url;?>" readonly data-toggle="tooltip" title="Copy the link to this Scribble.">
				</div>

				<span id="share-btn-container" <?php if ($ukey==""):?>style="display:none;"<?php endif;?>>
					<button id="fb-share-btn-big" class="zocial facebook" style="vertical-align: top;" onclick="ShareToFacebook(<?php echo $id;?>)">
						Share
					</button>
					<!-- <button id="fb-share-btn-small" class="zocial icon facebook" style="vertical-align:top;" data-toggle="tooltip" title="Share this Scribble on Facebook" onclick="ShareToFacebook(<?php echo $id;?>)"></button> -->
				</span>

				<div class="btn-group" id="btn-container-1" style="float:right;">
					<button class="btn btn-primary" id="save-scribble-btn" data-toggle="tooltip" title="Scribble!" onclick="jqAjaxSave(<?php echo $id;?>)"><i class="icon-save"></i></button>
					<button class="btn" data-toggle="tooltip" title="Add a new Scribble" id="add-scribble-btn" onclick="window.location.href = 'index.php'"><i class="icon-plus"></i></button>
					<button class="btn" id="toggle-btn" data-toggle="tooltip" title="Toggle Edit/View Mode" onclick="ToggleEditViewMode()"><i class="icon-edit"></i></button>
					<button class="btn" id="toggle-live-preview" data-toggle="tooltip" title="Toggle Live Preview" onclick="ToggleLivePreview()"><i class="icon-columns"></i></button>
					<button id="refresh-scribble-btn" class="btn <?php if ($ukey == ""):?>disabled<?php endif;?>" data-toggle="tooltip" title="Refresh" onclick="jqAjaxRefresh(<?php echo $id;?>)"><i class="icon-refresh"></i></button>
				</div>
			</div>
		</div>

		<div class="row-fluid" id="btn-container-2">
			<div class="span8 offset2">
				<div class="btn-group">
					<button class="btn btn-primary" id="save-scribble-btn-2" data-toggle="tooltip" title="Scribble!" onclick="jqAjaxSave(<?php echo $id;?>)"><i class="icon-save"></i></button>
					<button class="btn" data-toggle="tooltip" title="Add a new Scribble" id="add-scribble-btn-2" onclick="window.location.href = 'index.php'"><i class="icon-plus"></i></button>
					<button class="btn" id="toggle-btn-2" data-toggle="tooltip" title="Toggle Edit/View Mode" onclick="ToggleEditViewMode()"><i class="icon-edit"></i></button>
					<button id="refresh-scribble-btn-2" class="btn <?php if ($ukey == ""):?>disabled<?php endif;?>" data-toggle="tooltip" title="Refresh" onclick="jqAjaxRefresh(<?php echo $id;?>)"><i class="icon-refresh"></i></button>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span8 offset2">
				<input id="scribble-title" type="text" class="span10 offset1" placeholder="Title" value="<?php echo $title;?>">
			</div>
		</div>

		<div class="row-fluid">
			<textarea class="span8 offset2" style="height:300px;display:none;float:left;" id="scribble-content" placeholder="The quick brown fox jumps over the lazy dog." onkeyup="KeyUpChanges()"><?php echo $content;?></textarea>
			<div class="span4" id="scribble-live-preview" style="display:none;"><?php echo $content;?></div>
		</div>
		<div class="row-fluid">
			<div class="span8 offset2" id="scribble-view"><?php echo $content;?></div>
		</div>

	</body>
</html>