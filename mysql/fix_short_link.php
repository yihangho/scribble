<?php
include 'mysql.php';
$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."scribble");
while ($row = $result->fetch_assoc())
{
	$short_link = $row['short_url'];
	$id = $row['id'];
	if (strpos("goo.gl", $short_link) === false)
	{
		$long_url = APPS_HOME.$row['ukey'];

		$post_data = array(
			"longUrl" => $long_url,
			"key" => GOOGLE_API
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/urlshortener/v1/url");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
		$google_response = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($google_response, true);
		$short_url = $response['id'];
		$mysql_db->query("UPDATE ".MYSQL_PREFIX."scribble SET short_url='$short_url' WHERE id='$id'");
	}
}
$result->free_result();
?>
<!doctype html>
<html>
<head>
	<title>Setup</title>
</head>
<body>
	Links fixed.
</body>
</html>