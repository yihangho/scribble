<?php
define("MYSQL_HOST", "localhost");
define("MYSQL_USER", "scribble");
define("MYSQL_PASSWORD", "scribble");
define("MYSQL_DATABASE", "scribble");
// define("MYSQL_PORT", "8888");
define("MYSQL_PREFIX", "");

if (defined("MYSQL_PORT"))
	$mysql_db = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
else
	$mysql_db = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE);

if ($mysql_db->connect_error)
	die("MySQL connection error: ".$mysql_db->connect_error);

define("APPS_HOME", "http://www.scribble.loc/");

define("FB_APPID", "111111111111111");
define("GOOGLE_API", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

define("URL_REWRITE", true);
?>