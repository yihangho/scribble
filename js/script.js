var Preview = {
	delay: 150,
	live_preview: "#scribble-live-preview",
	viewer: "#scribble-view",
	live_preview_obj: document.getElementById("#scribble-live-preview"),
	viewer_obj: document.getElementById("#scribble-view"),
	editor: "#scribble-content",
	timeout: null,
	mjRunning: false,
	oldText: null,

// 	Init: function () {
// 		this.live_preview = "#scribble-live-preview";
// 		this.viewer = "#scribble-view";
// 		this.editor = "#scribble-content";
// 		this.live_preview_obj = document.getElementById("#scribble-live-preview");
// 		this.viewer_obj = document.getElementById("#scribble-view");
// 	},

	Update: function () {
		if (this.timeout) {clearTimeout(this.timeout)}
		this.timeout = setTimeout(this.callback,this.delay);
	},

	CreatePreview: function () {
		Preview.timeout = null;
		if (this.mjRunning) return;
		var text = $(this.editor).val();
		if (text === this.oldtext) return;
		$(this.live_preview).html(this.oldtext = text);
		$(this.viewer).html(text);
		this.mjRunning = true;
		MathJax.Hub.Queue(
			["Typeset",MathJax.Hub,this.live_preview_obj],
			["Typeset",MathJax.Hub,this.viewer_obj],
			["PreviewDone",this]
		);
	},

	PreviewDone: function () {
		this.mjRunning = false;
	}
};

Preview.callback = MathJax.Callback(["CreatePreview",Preview]);
Preview.callback.autoReset = true;
FB.init({appId: "285916588207824", status: true, cookie: true});

function ShareToFacebook(idx)
{
	if (idx == undefined) return;
	if ($("#fb-share-btn-big").hasClass("disabled")) return;
	$("#fb-share-btn-big").addClass("disabled");
	$("#fb-share-btn-small").addClass("disabled");

	$.ajax({
		type: "POST",
		url: "ajax/refresh.php",
		data: {
			id: idx
		},
		success: function(data){
			if (data.status == "pass")
			{
				var obj =
				{
					method: "feed",
					link: "http://www.scribble.loc/"+data.ukey,
					name: data.title+" - Scribble",
					caption: "Write math and share it with Scribble!",
					description: data.content.substring(0, Math.min(200, data.content.length))
				};
				$("#fb-share-btn-big").removeClass("disabled");
				$("#fb-share-btn-small").removeClass("disabled");
				FB.ui(obj);
			}
			else
				window.location.href = "index.php";
		},
		dataType: "json"
	});
}

function postToFeed() {

	var obj = {
		method: 'feed',
		link: 'https://developers.facebook.com/docs/reference/dialogs/',
		name: 'Facebook Dialogs',
		caption: 'Reference Documentation',
		description: 'Using Dialogs to interact with users.'
	};

	function callback(response) {
// 		document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
	}

	FB.ui(obj, callback);
}

function KeyUpChanges()
{
	$("#save-scribble-btn").removeClass("btn-success");
	$("#save-scribble-btn").html('<i class="icon-save"></i>');
	$("#save-scribble-btn-2").removeClass("btn-success");
	$("#save-scribble-btn-2").html('<i class="icon-save"></i>');
	Preview.CreatePreview();
}

function ToggleEditViewMode()
{
	if ($("#toggle-btn").hasClass("disabled"))
		return;
	if ($("#toggle-btn i").hasClass("icon-edit"))
	{
		$("#scribble-view").hide();
		$("#scribble-content").show();
		$("#toggle-btn i").removeClass("icon-edit");
		$("#toggle-btn i").addClass("icon-search");
		$("#toggle-btn-2 i").removeClass("icon-edit");
		$("#toggle-btn-2 i").addClass("icon-search");
	}
	else
	{
		$("#scribble-view").show();
		$("#scribble-content").hide();
		$("#toggle-btn i").removeClass("icon-search");
		$("#toggle-btn i").addClass("icon-edit");
		$("#toggle-btn-2 i").removeClass("icon-search");
		$("#toggle-btn-2 i").addClass("icon-edit");
	}
}

function ToggleLivePreview()
{
	if ($("#toggle-live-preview").hasClass("btn-success"))
	{
		$("#toggle-live-preview").removeClass("btn-success");
		$("#scribble-content").removeClass("span4");
		$("#scribble-content").addClass("span8");
		$("#toggle-btn").removeClass("disabled");
		$("#scribble-content").hide();
		$("#scribble-live-preview").hide();
		if ($("#toggle-btn i").hasClass("icon-edit"))
			$("#scribble-view").show();
		else
			$("#scribble-content").show();
	}
	else
	{
		$("#toggle-live-preview").addClass("btn-success");
		$("#scribble-content").removeClass("span8");
		$("#scribble-content").addClass("span4");
		$("#toggle-btn").addClass("disabled");
		$("#scribble-content").show();
		$("#scribble-live-preview").show();
		$("#scribble-view").hide();
	}
}

function jqAjaxRefresh(idx)
{
	if ($("#refresh-scribble-btn").hasClass("disabled"))
		return;

	if (idx == undefined) return;

	$("#refresh-scribble-btn").addClass("disabled");
	$("#refresh-scribble-btn-2").addClass("disabled");

	$.ajax({
		type: "POST",
		url: "ajax/refresh.php",
		data: {
			id: idx
		},
		success: function(data){
			if (data.status == "pass")
			{
				document.title = "Scribble - " + data.title;
				$("#scribble-title").val(data.title);
				$("#scribble-content").val(data.content);
				$("#refresh-scribble-btn").removeClass("disabled");
				$("#refresh-scribble-btn-2").removeClass("disabled");
				Preview.Update();
			}
			else
				window.location.href = "index.php";
		},
		dataType: "json"
	});
}

function jqAjaxSave(idx)
{
	if ($("#scribble-save").hasClass("disabled"))
		return;

	$("#save-scribble-btn").addClass("disabled");
	$("#save-scribble-btn-2").addClass("disabled");
	$("#save-scribble-btn").html('<i class="icon-spinner icon-spin"></i>');
	$("#save-scribble-btn-2").html('<i class="icon-spinner icon-spin"></i>');


	if (idx == undefined) idx = -1;

	$.ajax({
		type: "POST",
		url: "ajax/save.php",
		data: {
			id: idx,
			title: $("#scribble-title").val(),
			content: $("#scribble-content").val()
		},
		success: function(data){
			if (data.status == "pass")
			{
				document.title = "Scribble - " + data.title;
				$("#scribble-title").val(data.title);
				$("#scribble-content").val(data.content);
				Preview.Update();

				$("#save-scribble-btn").html('<i class="icon-ok"></i>');
				$("#save-scribble-btn").removeClass("disabled");
				$("#save-scribble-btn").addClass("btn-success");
				$("#save-scribble-btn").attr("onclick", "jqAjaxSave("+data.id+")");
				$("#save-scribble-btn-2").html('<i class="icon-ok"></i>');
				$("#save-scribble-btn-2").removeClass("disabled");
				$("#save-scribble-btn-2").addClass("btn-success");
				$("#save-scribble-btn-2").attr("onclick", "jqAjaxSave("+data.id+")");

				$("#refresh-scribble-btn").removeClass("disabled");
				$("#refresh-scribble-btn").attr("onclick", "jqAjaxRefresh("+data.id+")");
				$("#refresh-scribble-btn-2").removeClass("disabled");
				$("#refresh-scribble-btn-2").attr("onclick", "jqAjaxRefresh("+data.id+")");

				$("#short-url").val(data.short_url);
				$("#short-url-container").show();

				$("#share-btn-container").show();
				$("#fb-share-btn-big").attr("onclick", "ShareToFacebook("+data.id+")");
				$("#fb-share-btn-small").attr("onclick", "ShareToFacebook("+data.id+")");
			}
			else
			{
				$("#scribble-save").html('Not Scribbled <i class="icon-remove"></i>');
				$("#scribble-save").removeClass("disabled");
				$("#scribble-save").addClass("btn-danger");
				setTimeout(function(){
					$("#scribble-save").html('Scribble <i class="icon-save"></i>');
					$("#scribble-save").removeClass("btn-danger");
				}, 2500);
			}
		},
		dataType: "json"
	});
}

$(document).ready(function(){
	$("#short-url").tooltip({'placement': 'bottom', 'container': '#tooltip-container', 'trigger': 'hover'});
// 	$("#fb-share-btn-small").tooltip({'placement': 'bottom', 'container': '#tooltip-container', 'trigger': 'hover'});
	$("#add-scribble-btn").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});
	$("#toggle-btn").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});
	$("#save-scribble-btn").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});
	$("#toggle-live-preview").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});
	$("#refresh-scribble-btn").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});
// 	$("#add-scribble-btn-2").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});
// 	$("#toggle-btn-2").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});
// 	$("#save-scribble-btn-2").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});
// 	$("#refresh-scribble-btn-2").tooltip({'placement': 'bottom', 'container': '#tooltip-container'});

	if (editor_mode) ToggleEditViewMode();

	$("#short-url").click(function(){
		$(this).focus();
		$(this).select();
	});

	$(window).resize(function(){
		if ($(window).width() <= 475 && $("#toggle-live-preview").hasClass("btn-success"))
			ToggleLivePreview();
	});
});